use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :api, Api.Endpoint,
  http: [port: 4000],
  server: false


config :api, Api.Auth.Guardian,
  # allowed_algos: ["HS512"], # optional
  # verify_module: Guardian.JWT,  # optional
  ttl: { 30, :days },
  allowed_drift: 2000,
  verify_issuer: true, # optional
  issuer: "Api",
  #secret_key: "2wehpOqc8qKtozsjCh4QieDYiLjvMxiUr6XIP2bKLs8CJHHF/iKeY36XxxacwVUY", # Use mix guardian.gen.secret
  secret_key: "jim",
  serializer: Api.Auth.GuardianSerializer,
  module: Api.Auth.Guardian,
  error_handler: Api.Auth.ErrorHandler