defmodule Api.SessionView do
  use Api, :view

  def render("sign_in.json", %{user: user, jwt: jwt}) do
    %{
      status: :ok,
      data: %{
        token: jwt,
        email: user.email
      },
    message: "You are successfully logged in ! Add this token to authorization header to make authorized requests"
    }
  end


  def render("show.json", %{user: user, jwt: jwt}) do
    %{
      data: render_one(user, Back.UserView, "user.json"),
      meta: %{token: jwt}
    }
  end

  def render("error.json", _), do:
    %{error: "invalid email or password"}

  def render("delete.json", _), do:
    %{ok: true}

  def render("forbidden.json", %{error: error}), do:
    %{error: error}


end
