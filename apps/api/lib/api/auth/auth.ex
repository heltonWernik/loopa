defmodule Api.Auth do
  @moduledoc """
  This context is an UTIL one, to keep utility files used by Guardian.
  Auth functions are in the Account context.

  For more info see:
  https://gist.github.com/nikneroz/ba698a120bc037801f45328855cbdca8
  https://github.com/eteubert/phoenix-auth-demo/blob/master/lib/auth/guardian/guardian.ex
  https://gist.github.com/nikneroz/2363815cf6d66a8d78ed79a4d6a75203
  https://medium.com/@tylerpachal/session-authentication-example-for-phoenix-1-3-using-guardian-1-0-beta-a228c78478e6
  https://gist.github.com/nikneroz/2363815cf6d66a8d78ed79a4d6a75203
  https://medium.freecodecamp.org/authentication-using-elixir-phoenix-f9c162b2c398
  https://github.com/ueberauth/guardian
  https://medium.freecodecamp.org/authentication-using-elixir-phoenix-f9c162b2c398
  https://elixirschool.com/en/lessons/libraries/guardian/
  """
end
