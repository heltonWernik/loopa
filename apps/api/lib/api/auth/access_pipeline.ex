defmodule Api.Auth.AccessPipeline do
  use Guardian.Plug.Pipeline, otp_app: :api

  #  attempt to load the current user if there was a valid jwt in the request
  plug Guardian.Plug.Pipeline, module: Api.Auth.Guardian, error_handler: Api.Auth.ErrorHandler
  # look for a jwt in an Authorization: Bearer header, and validate it.
  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.VerifySession
  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.LoadResource, allow_blank: true

end
