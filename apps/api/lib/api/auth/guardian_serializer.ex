defmodule Api.Auth.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias Db.Repo
  alias Db.Account.User

  def for_token(user = %User{}), do: {:ok, "User:#{user.id}"}
  def for_token(_), do: {:error, "Unkonwn resource type"}

  def from_token("User:" <> id), do: {:ok, Repo.get(User, String.to_integer(id))}
  def from_token(_), do: {:error, "Unkonwn resource type"}


end

