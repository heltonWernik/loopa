defmodule Api.RegistrationController do
  use Api, :controller
  alias Db.Account
  alias Db.Account.User
  require IEx


  action_fallback Api.FallbackController


  def sign_up(conn, params) do
    with {:ok, %User{} = user} <- Account.register(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("success.json", user: user)
    end
  end



end

