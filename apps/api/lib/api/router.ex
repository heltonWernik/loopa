defmodule Api.Router do
  use Api, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug Api.Auth.AccessPipeline
  end

  pipeline :auth do
    plug Api.Auth.AccessPipeline
  end

  pipeline :authenticated do
    plug Guardian.Plug.EnsureAuthenticated, error_handler: Api.Auth.ErrorHandler
  end

  scope "/api", Api do
  #scope "/api", Api, as: :api do
    pipe_through :api
    post "/sessions", SessionController, :create
    delete "/sessions", SessionController, :delete
    post "/sessions/refresh", SessionController, :refresh

    # from https://gist.github.com/nikneroz/2363815cf6d66a8d78ed79a4d6a75203
    post "/sign_up", RegistrationController, :sign_up
    post "/sign_in", SessionController, :sign_in

    pipe_through :authenticated
    resources "/users", UserController, except: [:new, :edit]

  end
end
