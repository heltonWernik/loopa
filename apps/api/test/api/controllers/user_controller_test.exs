# defmodule Api.UserControllerTest do
#   use Api.ConnCase

#   alias Db.Account
#   alias Db.Account.User

#   @create_attrs %{activated: true, email: "some email", first_name: "some first_name", language: "some language", last_name: "some last_name", password_hash: "some password_hash", role: []}
#   @update_attrs %{activated: false, email: "some updated email", first_name: "some updated first_name", language: "some updated language", last_name: "some updated last_name", password_hash: "some updated password_hash", role: []}
#   @invalid_attrs %{activated: nil, email: nil, first_name: nil, language: nil, last_name: nil, password_hash: nil, role: nil}

#   def fixture(:user) do
#     {:ok, user} = Account.create_user(@create_attrs)
#     user
#   end

#   setup %{conn: conn} do
#     {:ok, conn: put_req_header(conn, "accept", "application/json")}
#   end

#   describe "index" do
#     test "lists all users", %{conn: conn} do
#       conn = get conn, user_path(conn, :index)
#       assert json_response(conn, 200)["data"] == []
#     end
#   end

#   describe "create user" do
#     test "renders user when data is valid", %{conn: conn} do
#       conn = post conn, user_path(conn, :create), user: @create_attrs
#       assert %{"id" => id} = json_response(conn, 201)["data"]

#       conn = get conn, user_path(conn, :show, id)
#       assert json_response(conn, 200)["data"] == %{
#         "id" => id,
#         "activated" => true,
#         "email" => "some email",
#         "first_name" => "some first_name",
#         "language" => "some language",
#         "last_name" => "some last_name",
#         "password_hash" => "some password_hash",
#         "role" => []}
#     end

#     test "renders errors when data is invalid", %{conn: conn} do
#       conn = post conn, user_path(conn, :create), user: @invalid_attrs
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "update user" do
#     setup [:create_user]

#     test "renders user when data is valid", %{conn: conn, user: %User{id: id} = user} do
#       conn = put conn, user_path(conn, :update, user), user: @update_attrs
#       assert %{"id" => ^id} = json_response(conn, 200)["data"]

#       conn = get conn, user_path(conn, :show, id)
#       assert json_response(conn, 200)["data"] == %{
#         "id" => id,
#         "activated" => false,
#         "email" => "some updated email",
#         "first_name" => "some updated first_name",
#         "language" => "some updated language",
#         "last_name" => "some updated last_name",
#         "password_hash" => "some updated password_hash",
#         "role" => []}
#     end

#     test "renders errors when data is invalid", %{conn: conn, user: user} do
#       conn = put conn, user_path(conn, :update, user), user: @invalid_attrs
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "delete user" do
#     setup [:create_user]

#     test "deletes chosen user", %{conn: conn, user: user} do
#       conn = delete conn, user_path(conn, :delete, user)
#       assert response(conn, 204)
#       assert_error_sent 404, fn ->
#         get conn, user_path(conn, :show, user)
#       end
#     end
#   end

#   defp create_user(_) do
#     user = fixture(:user)
#     {:ok, user: user}
#   end
# end
