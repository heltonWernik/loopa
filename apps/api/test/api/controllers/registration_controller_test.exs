defmodule Api.RegistrationControllerTest do
  use Api.ConnCase

  require IEx

  @create_attrs %{
        username: "heltonwenik",
        password: "12345678"
      }

#   @update_attrs %{
#   }

  @invalid_attrs %{
    username: nil,
    password: nil
  }


  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end


  describe "sign_up" do
    test "sign_up when data is valid", %{conn: conn} do
      conn = post(conn, registration_path(conn, :sign_up), @create_attrs)

      assert %{"message" => "Now you can sign in using your email and password at /api/sign_in.\nYou will receive JWT token.\nPlease put this token into Authorization header for all authorized requests.\n",
        "status" => "ok"} = json_response(conn, 201)

    end
  end

  test "sign_up renders errors when data is invalid", %{conn: conn} do
    conn = post(conn, registration_path(conn, :sign_up), @invalid_attrs)
    assert json_response(conn, 422)["errors"] != %{}
  end

end
