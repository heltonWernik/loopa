defmodule Api.ErrorHelpersTest do
  use Api.ConnCase, async: true
  require IEx

  # Bring render/3 and render_to_string/3 for testing custom views

  test "full_errors with one error" do
    changeset_errors = [email: {"has already been taken", []}]
    assert ["email has already been taken, "] == Api.ErrorHelpers.full_errors(changeset_errors)
  end

end
