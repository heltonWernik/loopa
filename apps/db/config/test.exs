use Mix.Config

# Configure your database
config :db, Db.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "db_test",
  hostname: "localhost",
  ownership_timeout: 600_000,
  pool: Ecto.Adapters.SQL.Sandbox
  
