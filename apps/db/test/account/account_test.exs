defmodule Db.AccountTest do
  use Db.DataCase
  require IEx
  alias Db.Account

  describe "users" do
    alias Db.Account.User

      @valid_attrs %{
        username: "heltonwernik",
        password: "12345678",
      }

      @invalid_attrs %{
        username: nil,
        password: nil
      }

      @valid_login %{username: "heltonwernik", password: "12345678"}
      @invalid_login %{username: "heltonwernik", password: "123"}


    test "register/1 with valid data register a user" do
      assert {:ok, %User{} = user} = Account.register(@valid_attrs)
      assert user.username == "heltonwernik"
      assert user.password == "12345678"
      assert String.slice(user.password_hash, 0..6) == "$argon2"
    end

    test "register/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.register(@invalid_attrs)
    end

    test "register/1 with invalid password returns error changeset and details" do
      # invalid_passord = %{email: "helton@wernik.com", language: "en", password: "12", role: "patient", first_name: "Helton", last_name: "Wernik"}
      assert {:error, error} = Account.register(@invalid_login)
      assert error.errors == [password: {"should be at least %{count} character(s)",[count: 6, validation: :length, min: 6]}]
    end

    test "authenticate/1 with valid email and password" do
      Account.register(@valid_attrs)
      assert {:ok, %User{}} = Db.Account.authenticate(@valid_login)
    end

    test "authenticate/1 with invalid password" do
      Account.register(@valid_attrs)
      assert {:error, :login_failed} = Db.Account.authenticate(@invalid_login)
    end


  end
end
