defmodule Db.Account.User do
    use Ecto.Schema
    import Ecto.Changeset
    alias Db.Repo
    alias Db.Account.User
    require IEx

    schema "users" do
        field :username , :string
        field :password , :string, virtual: true
        field :password_hash, :string

        timestamps()
      end

      def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, [:username, :password_hash])
        |> validate_required([:username])
        |> validate_length(:username, min: 5, max: 15)
        |> validate_confirmation(:password, message: "Password does not match")
        |> unique_constraint(:username, message: "Username already taken")
    end

  @doc "Used in the sign_up flow, when calling the /api/sign_up"
  def registration_changeset(struct, params) do
    struct
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_length(:password, min: 6, max: 100)
    #|> validate_format(:password, ~r/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).*/, [message: "Must include at least one lowercase letter, one uppercase letter, and one digit"])
    |> encrypt_password()
  end

  # Extract password change from changeset, ecnrypt it and put it on password_hash field
  defp encrypt_password(changeset) do
    password = changeset |> get_change(:password)
    if password do
      changeset
      |> put_change(:password_hash, Comeonin.Argon2.hashpwsalt(password))
    else
      changeset
    end
  end


  @doc "Find and confirm password"
  def authenticate(%{username: username, password: password}) do
    IEx.pry
    user = Repo.get_by(User, username: String.downcase(username))
    case check_password(user, password) do
      true -> {:ok, user}
      _    -> {:error, :login_failed}
    end
  end


  defp check_password(user, password) do
    case user do
      nil -> Comeonin.Argon2.dummy_checkpw()
      _   -> Comeonin.Argon2.checkpw(password, user.password_hash)
    end
  end

end
