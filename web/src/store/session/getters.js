export default {
  currentUser: (state) => state.currentUser,
  sessionError: (state) => state.sessionError,
  registrationErrors: (state) => state.registrationErrors,
  changePasswordErrors: (state) => state.changePasswordErrors
}

// export default totals (state) {
//     if (state.price)  {
//         return state.currentUser
//     }
// }