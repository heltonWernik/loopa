import { Socket } from 'phoenix-elixir'
import store from '../store'
import { socketURL } from '../../../config/url'
import uniqueId from 'uniqid'
import * as types from '../mutation-types'

export const socket = new Socket(socketURL)

export function joinUserChannel({ id, jwt }) {
    let userChannel = socket.channel(`users:${id}`, { token: jwt })
  
    userChannel.join()
      .receive('ok', _ => {
        store.dispatch('addAlert', {
          id: uniqueId('alert_'),
          type: 'success',
          message: 'User joined succesfully!'
        })
      })
  
    userChannel.on('user:add_notification', notification => {
      store.commit(types.ADD_NOTIFICATION, notification)
      store.dispatch('addAlert', {
        id: uniqueId('alert_'),
        type: 'info',
        notification: notification,
        message: `${notification.username} commented in your post`
      })
    })
  
    userChannel.on('user:delete_notification', _ => {
      store.dispatch('getNotifications')
    })
  
    return userChannel
  }