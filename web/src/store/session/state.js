export default {
  currentUser: null,
  sessionError: null,
  registrationErrors: {},
  changePasswordErrors: {}
}
