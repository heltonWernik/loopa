import * as types from '../mutation-types'

export default {
    [types.SET_CURRENT_USER](state, user) {
      state.currentUser = { ...state.currentUser, ...user }
    },
  
    [types.USER_SIGNED_OUT](state) {
      state.currentUser = null
    },
  
    [types.SET_SESSION_ERROR](state, error) {
      state.sessionError = error
    },
  
    [types.CLEAR_SESSION_ERROR](state) {
      state.sessionError = null
    },
  
    [types.SET_REGISTRATIONS_ERRORS](state, errors) {
      state.registrationErrors = errors
    },
  
    [types.CLEAR_REGISTRATIONS_ERRORS](state) {
      state.registrationErrors = {}
    },
  
    [types.SET_CHANGE_PASSWORD_ERRORS](state, errors) {
      state.changePasswordErrors = errors
    },
  
    [types.CLEAR_CHANGE_PASSWORD_ERRORS](state) {
      state.changePasswordErrors = {}
    },
  }